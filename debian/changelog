ros-rosconsole-bridge (0.5.4-5) unstable; urgency=medium

  [ Jochen Sprickerhof ]
  * Drop space

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository-Browse.
  * Set upstream metadata fields: Repository.

  [ Timo Röhling ]
  * Wrap and sort Debian package files
  * Migrate to dh-ros
  * Bump Standards-Version to 4.7.0
  * Add myself to uploaders

 -- Timo Röhling <roehling@debian.org>  Wed, 25 Sep 2024 21:50:14 +0200

ros-rosconsole-bridge (0.5.4-4) unstable; urgency=medium

  * Team upload.
  * Upload to unstable
  * Fix Multiarch hinter issues

 -- Timo Röhling <roehling@debian.org>  Wed, 22 Sep 2021 22:44:16 +0200

ros-rosconsole-bridge (0.5.4-3) experimental; urgency=medium

  * Team upload.
  * Move pkg-config and CMake config files to /usr/lib/<triplet>
  * Bump Standards-Version to 4.6.0 (no changes)

 -- Timo Röhling <roehling@debian.org>  Sun, 05 Sep 2021 21:32:18 +0200

ros-rosconsole-bridge (0.5.4-2) unstable; urgency=medium

  * simplify packaging

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 19 Dec 2020 13:00:48 +0100

ros-rosconsole-bridge (0.5.4-1) unstable; urgency=medium

  * simplify d/watch
  * New upstream version 0.5.4
  * Remove Thomas from Uploaders, thanks for working on this
  * bump policy and debhelper versions

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 06 Jun 2020 20:53:25 +0200

ros-rosconsole-bridge (0.5.3-1) unstable; urgency=medium

  * New upstream version 0.5.3
  * Update build dependencies
  * switch to debhelper-compat and debhelper 12
  * Bump policy version (no changes)
  * cleanup rules
  * add Salsa CI

 -- Jochen Sprickerhof <jspricke@debian.org>  Thu, 22 Aug 2019 09:57:15 +0200

ros-rosconsole-bridge (0.5.2-1) unstable; urgency=medium

  * New upstream version 0.5.2
  * Bump policy version (no changes)

 -- Jochen Sprickerhof <jspricke@debian.org>  Thu, 23 Aug 2018 18:51:52 +0200

ros-rosconsole-bridge (0.5.1-2) unstable; urgency=medium

  * Update Vcs URLs to salsa.d.o
  * Add R³
  * http -> https
  * Bump policy and debhelper versions

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 22 Jun 2018 17:41:11 +0200

ros-rosconsole-bridge (0.5.1-1) unstable; urgency=medium

  * New upstream version 0.5.1

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 10 Nov 2017 13:23:11 +0100

ros-rosconsole-bridge (0.5.0-1) unstable; urgency=medium

  * Update Vcs URLs
  * Update my email address
  * Update watch file
  * Remove old catkin version
  * New upstream version 0.5.0
  * Update compat to 10 (no changes)
  * Bump policy version (no changes)
  * Drop unused build dependency

 -- Jochen Sprickerhof <jspricke@debian.org>  Sun, 29 Oct 2017 08:36:54 +0100

ros-rosconsole-bridge (0.4.4-1) unstable; urgency=medium

  * Imported Upstream version 0.4.4
  * Remove patch applied upstream
  * Disable test suite for now

 -- Jochen Sprickerhof <debian@jochen.sprickerhof.de>  Sun, 10 Jul 2016 10:20:16 +0200

ros-rosconsole-bridge (0.4.2-2) unstable; urgency=medium

  * Add dependencies

 -- Jochen Sprickerhof <debian@jochen.sprickerhof.de>  Sun, 10 Jan 2016 15:04:11 +0100

ros-rosconsole-bridge (0.4.2-1) unstable; urgency=medium

  * Initial release (Closes: #804029)

 -- Jochen Sprickerhof <debian@jochen.sprickerhof.de>  Thu, 24 Dec 2015 01:59:58 +0000
